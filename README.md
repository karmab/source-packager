# Source Extractor

This analyser will read OpenShift buildlogs and figure out what source code went into the final container image.

## Advantages...

Decoupling source extraction from the build process will schedule workloads more efficiently and not slow down the Build.

## Drawbacks... 

It might that the git repository and commit hash disapear after the OpenShift Build finished and before source-extractors Build starts.

It might be that we dont see all commands which pull in source code, s2i assemble script may obfuscate some commands.

The source-extractor Build might run on a different set of enabled repositories or a different pip/npm/mvn mirror. Reconstructing src via `pip` will always involve a build, which is either intended nor possible (with the current s2c-python image).