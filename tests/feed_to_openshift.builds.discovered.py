#!/bin/env python

from kafka import KafkaProducer
from kafka.errors import KafkaError

import json
import logging
import os

producer = KafkaProducer(bootstrap_servers="localhost:9092",
                                      value_serializer=lambda m: json.dumps(
                                          m).encode('utf-8'),
                                      retries=5)


events_total = 0

with open('fixtures/build-events.json') as fixture:
    events = json.load(fixture)

    for event in events:
        producer.send('openshift.builds.discovered', event)
        events_total += 1
        
    producer.flush()

print("Send a total of %s events." % (events_total))